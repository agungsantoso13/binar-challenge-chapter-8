import './App.css';
import Page from './components/player/Page'

function App() {
  return (
    <div className="App">
      <h1>Player Page</h1>
      <Page/>
    </div>
  );
}

export default App;
