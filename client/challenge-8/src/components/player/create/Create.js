import React from 'react'

export default function Create(props) {
    
    
    return (
        <div className="container">
      <h3>Create Player</h3>
      <p>Isi form di bawah ini, untuk membuat Player baru!</p>
        <hr />
        <form>
            <div>
                <label>Username</label><br/>
                <input type="text" name="username" placeholder="masukkan username"></input>
            </div>
            <div>
                <label>Password</label><br/>
                <input type="password" name="password" placeholder="masukkan password"></input>
            </div>
            <div>
                <label>Email</label><br/>
                <input type="email" name="email" placeholder="masukkan email"></input>
            </div>
            <button className="BtnPage" type="submit">Submit</button>
        </form>      
    </div>
    )
}
