import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Home from './home/Home';
import Create from './create/Create';
import Update from './update/Update';
import Search from './search/Search';



export default function BasicExample() {
  return (
    <Router>
      <div>     
          <button className="BtnPage"><Link to="/">Home</Link></button>
          <button className="BtnPage"><Link to="/create">Create</Link></button>
          <button className="BtnPage"><Link to="/update">Update</Link></button>
          <button className="BtnPage"><Link to="/search">Search</Link></button>        
        
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route path="/create" component={Create}/>
          <Route path="/update" component={Update}/>
          <Route path="/search" component={Search}/>
        </Switch>
      </div>
    </Router>
  );
}
