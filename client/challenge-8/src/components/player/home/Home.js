import React from 'react'

export default function Home(props) {
    

    return (
        <div className="container">
            <h2>Selamat Datang</h2>
            <p>Ini adalah Halaman Home untuk Player. Anda dapat membuat akun Player baru, mengubah akun Player dan melakukan pencarian akun Player yang sudah terdaftar.</p>
            <br/><br/><br/>
            <p>**Selamat Mencoba**</p>
        </div>
    )
}
