import React from 'react'

export default function Update(props) {
    

    return (
        <div className="container">
      <h3>Update Player</h3>
      <p>Isi form di bawah ini, untuk mengubah akun Player!</p>
      <hr />
        <form>
            <div>
                <label>Username</label><br/>
                <input type="text" name="username" placeholder="masukkan username"></input>
            </div>
            <div>
                <label>Password</label><br/>
                <input type="password" name="password" placeholder="masukkan password"></input>
            </div>
            <div>
                <label>Email</label><br/>
                <input type="email" name="email" placeholder="masukkan email"></input>
            </div>
            <button className="BtnPage" type="submit">Submit</button>
        </form>    
    </div>
    )
}
